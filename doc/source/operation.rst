.. _operation:

Command Line Tools
==================

Usage
-----

log-processor-client
^^^^^^^^^^^^^^^^^^^^
.. program-output:: log-processor-client --help
   :nostderr:

log-processor-worker
^^^^^^^^^^^^^^^^^^^^
.. program-output:: log-processor-worker --help
   :nostderr:
